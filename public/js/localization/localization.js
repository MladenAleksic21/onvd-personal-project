import localization from "./content.js";

function localize() {
    let lang = document.documentElement.lang;

    document.querySelectorAll("[data-lang]").forEach(item => {
        item.innerHTML = localization[lang][item.getAttribute("data-lang")];
    });
}
function changeFlag() {
    document.getElementById("lang-switch-slier").classList.toggle("mk");
}

localize();

let sw = document.getElementById("lang-switch");
sw.addEventListener("change", e => {
    document.documentElement.lang = e.target.checked ? "mk" : "en";
    changeFlag();
    localize();
});