let localization = {
    "mk": {
        "title": "Младен Алексиќ",
        "about-me": "Јас сум студент на Финки, смер СИИС",
        "hero-header": "Дома",
        "menu-main-home": "Дома",
        "menu-main-about-me": "За мене",
        "menu-main-study": "Образование",
        "menu-main-skills": "Вештини",
        "header-about-me": "За Мене",
        "header-education": "Образование",
        "header-skills": "Вештини"
    }
    , "en": {
        "title": "Mladen Aleksic",
        "about-me": "I am a student in FINKI SIIS",
        "hero-header": "Home",
        "menu-main-home": "Home",
        "menu-main-about-me": "About Me",
        "menu-main-study": "Education",
        "menu-main-skills": "Skills",
        "header-about-me": "About Me",
        "header-education": "Education",
        "header-skills": "Skills"
    }
}


export default localization;