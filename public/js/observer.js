
//observer for rocket
let observer = new IntersectionObserver((entries, observer) => {
    if (entries[0].isIntersecting) {
        document.querySelector(".rider").style.marginLeft = "100%";
        observer.disconnect();
    }
}, {
    rootMargin: '0px',
    threshold: 1.0
});


//observer for skills
let didTheyInstersect = {
    "first-filler": 0,
    "last-filler": 0
};

let fillerObserver = new IntersectionObserver((entries, observer) => {
    entries.forEach(item => {
        if (item.isIntersecting) {
            console.log()
            if (item.target.id == "first-filler") {
                didTheyInstersect["first-filler"] = 1;
            } else if (item.target.id == "last-filler") {
                didTheyInstersect["last-filler"] = 1;
            }
            item.target.style.width = Math.floor(Math.random() * 100) + 70 + "%";
            if (didTheyInstersect["first-filler"] & didTheyInstersect["last-filler"]) {
                observer.disconnect();
            }
        }
    });
}, {
    rootMargin: '0px',
    threshold: 0.7
});


//observer for rocket that goes from right to left 
let absRocketObserver = new IntersectionObserver((entries, observer) => {
    entries.forEach(item => {
        if (item.isIntersecting) {
            document.querySelector(".abs-rider").style.right = "100%";
            document.querySelector(".abs-rider").style.top = "100vh";
            observer.disconnect();
        }
    })
}, {
    threshold: 0
});

let target = document.getElementById("rocket-pass");
observer.observe(target);

let fillers = document.querySelectorAll(".fill-color");
fillers.forEach(item => {
    fillerObserver.observe(item);
})

let absRocket = document.querySelector("#trigger-rocket");
absRocketObserver.observe(absRocket);